import React from 'react';

const AnotherComponent = () => {
    return (
        <>
            <p>Fragments provide a wrapper</p>
            <p>Useful in some cases!</p>
        </>
    )
};

export default AnotherComponent;
