import React from 'react';
import './css/AddTodo.css';
import TodoForm from './TodoForm';

const AddTodo = () => {
    return (
        <div className="addTodo container">
            <h3>Add Todo</h3>
            <TodoForm />
        </div>
    );
};

export default AddTodo;
