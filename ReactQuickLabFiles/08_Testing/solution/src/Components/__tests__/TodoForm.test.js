import React from "react";
import { shallow } from "enzyme";

import TodoForm from "../TodoForm";

describe(`<TodoForm>`, () => {
  it("should call the submitTodo function provided by props when form is submitted", () => {
    const props = { submitTodo: jest.fn() };
    const fakeEvent = { preventDefault: jest.fn() };

    const testComponent = shallow(<TodoForm {...props} />);

    testComponent.find("form").simulate("submit", fakeEvent);
    expect(props.submitTodo).toHaveBeenCalledTimes(1);
  });

  it("should not use the default behaviour when the form is submitted", () => {
    const props = { submitTodo: jest.fn() };
    const fakeEvent = { preventDefault: jest.fn() };

    const testComponent = shallow(<TodoForm {...props} />);

    testComponent.find("form").simulate("submit", fakeEvent);
    expect(fakeEvent.preventDefault).toHaveBeenCalledTimes(1);
  });
});
