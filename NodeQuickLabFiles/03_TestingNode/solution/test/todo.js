const mongoose = require('mongoose');
const Todo = require('../todo.model');

// Require the testing dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

const testData = require('./testData/sampleTodos.json');
const testDataArray = testData.todos;

chai.use(chaiHttp);
describe(`Testing requests on the database`, () => {
    beforeEach(done => {
        Todo.deleteMany({}, err => {
            if (err) done(err);
        });

        Todo.insertMany(testDataArray, (err, res) => {
            if (err) {
                console.info(`Error inserting`);
                done(err);
            } else {
                console.info(`Documents inserted`);
                done();
            }
        });
    });

    describe(`/GET todo`, () => {
        it(`it should GET all of the todos`, done => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(2);
                    done();
                });
        });
    });

    describe(`/POST create a todo`, () => {
        it(`should not create a todo without a description field`, done => {
            let todo = {
                todoDateCreated: `2019-05-27T00:00:00.000Z`,
                todoCompleted: false
            };

            chai.request(server)
                .post(`/add`)
                .send(todo)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`errors`);
                    res.body.errors.should.be.an(`array`);
                    done();
                });
        });

        it(`should create a todo that is properly formed`, done => {
            let todo = {
                todoDescription: `A test todo`,
                todoDateCreated: `2019-05-27T00:00:00.000Z`,
                todoCompleted: false
            };

            chai.request(server)
                .post(`/add`)
                .send(todo)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('todo').eql('todo added successfully');
                    done();
                });
        });
    });

    describe(`/GET/:id todo`, () => {
        it(`should GET a todo by the given id`, done => {
            const testId = testDataArray[0]._id;
            chai.request(server)
                .get(`/todo/${testId}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property(`_id`, testId);
                    done();
                });
        });
    });

    describe(`/POST/:id update existing todo`, () => {
        it(`should update a todo with POST for the given id`, done => {
            const todoToUpdate = testDataArray[0];
            todoToUpdate.todoCompleted = true;
            chai.request(server)
                .post(`/todo/${todoToUpdate._id}`)
                .send(todoToUpdate)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('string');
                    res.body.should.eql(`Todo updated!`);
                    done();
                });
        });

        it(`should return a 404 error if the todo to update is not found`, done => {
            chai.request(server)
                .post(`/todo/notAnId`)
                .send({})
                .end((err, res) => {
                    res.should.have.status(404);
                    res.text.should.eql(`That todo cannot be found`);
                    done();
                });
        });
    });
});