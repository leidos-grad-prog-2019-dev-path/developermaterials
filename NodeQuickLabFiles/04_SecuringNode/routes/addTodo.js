const express = require('express');
const router = express.Router();
const Todo = require('../todo.model');
const bodyParser = require('body-parser');
const cors = require('cors');

const { check, validationResult } = require('express-validator/check');

const isoDateRegExp = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/;

router.use(cors());
router.use(bodyParser.json());

// router.route(`/`).post((req, res) => {
//     // res.send(`Adding Todo successful`);
//     const todo = new Todo(req.body);
//     todo.save()
//         .then(todo => {
//             res.status(200).json({ 'todo': 'todo added successfully' });
//         })
//         .catch(err => res.status(400).send('Adding new todo failed'));
// });

router.route(`/`).post(
    [
        check('todoDescription').isString(),
        check('todoDateCreated').matches(isoDateRegExp),
        check('todoCompleted').isBoolean()
    ],
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        const todo = new Todo(req.body);
        todo.save()
            .then(todo => {
                res.status(200).json({ 'todo': 'todo added successfully' });
            })
            .catch(err => res.status(400).send('Adding new todo failed'));
    }
);

module.exports = router;